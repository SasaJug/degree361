package com.sasaj.degree361taskapp.provider;

public class Constants {
	
    /**
     * URL of the LastFm web service.
     */
    public static final String SERVER_URL = "http://ws.audioscrobbler.com/2.0";
}
