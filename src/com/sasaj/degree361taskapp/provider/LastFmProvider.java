package com.sasaj.degree361taskapp.provider;

import com.sasaj.degree361taskapp.provider.TrackContract.TrackEntry;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.util.Log;

public class LastFmProvider extends ContentProvider {
	
	public static enum Items { ONE,
		   					   MANY }
	
	/**
     * The URI Matcher used by this content provider.
     */
    private static final UriMatcher sUriMatcher = buildUriMatcher();

    /**
     * Use LastFmDatabaseHelper to manage database creation and version
     * management.
     */
    private LastFmDatabaseHelper mOpenHelper;

    /**
     * The code that is returned when a URI for more than 1 items/only 1 item is
     * matched against the given components.  Must be positive.
     */
    private static final int TRACKS_OVERALL = 100;
    private static final int TRACKS_OVERALL_ID = 101;
	
    private static final int TRACKS_7DAY = 200;
    private static final int TRACKS_7DAY_ID = 201;
	
    private static final int TRACKS_1MONTH = 300;
    private static final int TRACKS_1MONTH_ID = 301;
	
    private static final int TRACKS_3MONTH = 400;
    private static final int TRACKS_3MONTH_ID = 401;
	
    private static final int TRACKS_6MONTH = 500;
    private static final int TRACKS_6MONTH_ID = 501;
	
    private static final int TRACKS_12MONTH = 600;
    private static final int TRACKS_12MONTH_ID = 601;

	private final String TAG = getClass().getSimpleName();
	
	 /**
     * Helper method to match each URI to the VIDEO integers
     * constant defined above.
     * 
     * @return UriMatcher
     */
    private static UriMatcher buildUriMatcher() {
        // All paths added to the UriMatcher have a corresponding code
        // to return when a match is found.  The code passed into the
        // constructor represents the code to return for the rootURI.
        // It's common to use NO_MATCH as the code for this case.
        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);

        // The "Content authority" is a name for the entire content
        // provider, similar to the relationship between a domain name
        // and its website.  A convenient string to use for the
        // content authority is the package name for the app, which is
        // guaranteed to be unique on the device.
        final String authority = TrackContract.AUTHORITY;

        // For each type of URI that is added, a corresponding code is
        // created.
        matcher.addURI(authority,TrackEntry.TABLE_NAME_OVERALL,TRACKS_OVERALL);
        matcher.addURI(authority,TrackEntry.TABLE_NAME_OVERALL+"/#",TRACKS_OVERALL_ID);
        matcher.addURI(authority,TrackEntry.TABLE_NAME_SEVEN_DAY,TRACKS_7DAY);
        matcher.addURI(authority,TrackEntry.TABLE_NAME_SEVEN_DAY+"/#",TRACKS_7DAY_ID);
        matcher.addURI(authority,TrackEntry.TABLE_NAME_ONE_MONTH,TRACKS_1MONTH);
        matcher.addURI(authority,TrackEntry.TABLE_NAME_ONE_MONTH+"/#",TRACKS_1MONTH_ID);
        matcher.addURI(authority,TrackEntry.TABLE_NAME_THREE_MONTH,TRACKS_3MONTH);
        matcher.addURI(authority,TrackEntry.TABLE_NAME_THREE_MONTH+"/#",TRACKS_3MONTH_ID);
        matcher.addURI(authority,TrackEntry.TABLE_NAME_SIX_MONTH,TRACKS_6MONTH);
        matcher.addURI(authority,TrackEntry.TABLE_NAME_SIX_MONTH+"/#",TRACKS_6MONTH_ID);
        matcher.addURI(authority,TrackEntry.TABLE_NAME_TWELVE_MONTH,TRACKS_12MONTH);
        matcher.addURI(authority,TrackEntry.TABLE_NAME_TWELVE_MONTH+"/#",TRACKS_12MONTH_ID);

        return matcher;
    }
    

    /**
     * Hook method called when Database is created to initialize the
     * Database Helper that provides access to the Video Database.
     */
    @Override
    public boolean onCreate() {
        mOpenHelper = new LastFmDatabaseHelper(getContext());
        return true;
    }
    

    /**
     * Hook method called to handle requests for the MIME type of the
     * data at the given URI. The returned MIME type should start with
     * vnd.android.cursor.item for a single record, or
     * vnd.android.cursor.dir/ for multiple items
     */
    @Override
    public String getType(Uri uri) {
		
		UriData data = getDbAndItemNumber(uri);

        // Match the data.items to return appropriate
        // MIME_TYPE.
        switch (data.getItems()) {
        case MANY:
            return  "vnd.android.cursor.dir/"+ TrackContract.AUTHORITY + "/" + data.getTable();
        case ONE:
            return  "vnd.android.cursor.item/"+ TrackContract.AUTHORITY + "/" + data.getTable();
        default:
            throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
    }

    /**
     * Hook method to handle requests to delete one or more rows. The
     * implementation should apply the selection clause when
     * performing deletion, allowing the operation to affect multiple
     * rows in a directory.  As a courtesy, notifyChange() is called
     * after deleting.
     */
    @Override
    public int delete(Uri uri,
                      String selection,
                      String[] selectionArgs) {
    	// Create and/or open a database that will be used for reading
    	// and writing. Once opened successfully, the database is
    	// cached, so you can call this method every time you need to
    	// write to the database.
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();

        // Keeps track of the number of rows deleted.
        int rowsDeleted = 0;

        // Try to match against the path in a url.  It returns the
        // code for the matched node (added using addURI) or -1 if
        // there is no matched node.  If a match is found delete the
        // appropriate rows.
		
		UriData data = getDbAndItemNumber(uri);
        switch (data.getItems()) {
        case MANY:
            // code that deletes the row(s)
            // in the SQLite database table based on the parameters
            // passed into the method.
            rowsDeleted = db.delete(data.getTable(), selection, selectionArgs);
            break;
        default:
            throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

        // Notifies registered observers that rows were deleted and
        // attempt to sync changes to the network.
        if (selection == null || rowsDeleted != 0) 
        	Log.i(TAG, "Rows deleted: " + Integer.toString(rowsDeleted));
            getContext().getContentResolver().notifyChange(uri, 
                                                           null);
        return rowsDeleted;
    }


    
    // Hook method to handle requests to insert a set of new rows, or
    // the default implementation will iterate over the values and
    // call insert on each of them. As a courtesy, call notifyChange()
    // after inserting.
    @Override
    public int bulkInsert(Uri uri, ContentValues[] contentValues) {
        // Create and/or open a database that will be used for reading
        // and writing. Once opened successfully, the database is
        // cached, so you can call this method every time you need to
        // write to the database.
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
		
        // Try to match against the path in a uri.  It returns the
        // code for the matched node (added using addURI), or -1 if
        // there is no matched node.  If a match occurs update the
        // appropriate rows.
		
		UriData data = getDbAndItemNumber(uri);
        switch (data.getItems()) {
        case MANY:
            // Begins a transaction in EXCLUSIVE mode. 
            db.beginTransaction();
            int returnCount = 0;

            try {
                // TODO -- write the code that inserts all the
                // contentValues into the SQLite database.
            	for (ContentValues cv : contentValues){
            		db.insert(data.getTable(), TrackEntry.COLUMN_NAME, cv);
            		returnCount++;
            	}
                // Marks the current transaction as successful.
                db.setTransactionSuccessful();
            } finally {
                // End a transaction.
                db.endTransaction();
            }
            // Notifies registered observers that rows were updated
            // and attempt to sync changes to the network.
            getContext().getContentResolver().notifyChange(uri, null);
            return returnCount;
        default:
            return super.bulkInsert(uri, contentValues);
        }
    }

    
	 /**
    * Hook method called to handle requests to insert a new row.  As
    * a courtesy, notifyChange() is called after inserting.
    */
   @Override
   public Uri insert(Uri uri, ContentValues values) {
   	// Create and/or open a database that will be used for reading
       // and writing. Once opened successfully, the database is
       // cached, so you can call this method every time you need to
       // write to the database.
       final SQLiteDatabase db = mOpenHelper.getWritableDatabase();

       Uri returnUri;

       // Try to match against the path in a uri.  It returns the
       // code for the matched node (added using addURI), or -1 if
       // there is no matched node.  If there's a match insert a new
       // row.
		
		UriData data = getDbAndItemNumber(uri);
       switch (data.getItems()) {
       case MANY:
           // inserts a row in Table
           // and returns the row id.
           long id = db.insert(data.getTable(), TrackEntry.COLUMN_NAME, values);
           // Check if a new row is inserted or not.
           if (id > 0)
               returnUri = TrackEntry.buildTrackUri(data.getTable(),id);
           else
               throw new android.database.SQLException
                   ("Failed to insert row into " + uri);
           break;
       default:
           throw new UnsupportedOperationException("Unknown uri: " + uri);
       }

       // Notifies registered observers that a row was inserted and
       // attempt to sync changes to the network.
       getContext().getContentResolver().notifyChange(uri, null);
       return returnUri;
   }



   /**
    * Hook method called to handle query requests from clients.
    */
   @Override
   public Cursor query(Uri uri,
                       String[] projection,
                       String selection,
                       String[] selectionArgs,
                       String sortOrder) {
       Cursor retCursor;
       final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
       // Match the id returned by UriMatcher to query appropriate
       // table and rows.
		
	   UriData data = getDbAndItemNumber(uri);
       switch (data.getItems()) {
       case MANY: 
           // code to query the
           // entire SQLite database based on the parameters passed
           // into the method.
           retCursor = db.query(data.getTable(), projection, selection, selectionArgs, null, null, sortOrder);

           break;
       case ONE: 
           // Selection clause that matches server id with id passed
           // from Uri.
           final String rowId =
               ""
               + TrackEntry._ID
               + " = '"
               + ContentUris.parseId(uri)
               + "'";

           // code to query the
           // SQLite database for the particular rowId based on (a
           // subset of) the parameters passed into the method.
           
           SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
           qb.setTables(data.getTable());
           qb.appendWhere(rowId);
           retCursor = qb.query(db, projection, selection, selectionArgs, null, null, sortOrder);
           
           break;
       default:
           throw new UnsupportedOperationException("Unknown uri: " + uri);
       }

       // Register to watch a content URI for changes.
       retCursor.setNotificationUri(getContext().getContentResolver(),uri);
       
       return retCursor;
   }


   /**
    * Hook method called to handle requests to update one or more
    * rows. The implementation should update all rows matching the
    * selection to set the columns according to the provided values
    * map. As a courtesy, notifyChange() is called after updating .
    */
   @Override
   public int update(Uri uri,
                     ContentValues values,
                     String selection,
                     String[] selectionArgs) {
       // Create and/or open a database that will be used for reading
       // and writing. Once opened successfully, the database is
       // cached, so you can call this method every time you need to
       // write to the database.
       final SQLiteDatabase db =
           mOpenHelper.getWritableDatabase();

       int rowsUpdated;

       // Try to match against the path in a uri. If a match occurs update the
       // appropriate rows.
		UriData data = getDbAndItemNumber(uri);
       switch (data.getItems()) {
       case MANY:
           // Updates the rows in the Database and returns no of rows
           // updated.
           rowsUpdated = db.update(data.getTable(), values, selection, selectionArgs);
           break;
       default:
           throw new UnsupportedOperationException("Unknown uri: " + uri);
       }

       // Notifies registered observers that rows were updated and
       // attempt to sync changes to the network.
       if (rowsUpdated != 0) 
           getContext().getContentResolver().notifyChange(uri, null);
       
       return rowsUpdated;
   }
	
	
   /**
    * Method called first from all CRUD methods. 
	 * Returns table name and number of items that match given Uri. 
	 * 
	 * @param uri
	 * @return uriData
    */
	private UriData getDbAndItemNumber(Uri uri){
		final int match = sUriMatcher.match(uri);
		switch(match){
			
			case TRACKS_OVERALL:
				return new UriData(TrackEntry.TABLE_NAME_OVERALL, Items.MANY);
				
			case TRACKS_OVERALL_ID:
				return new UriData(TrackEntry.TABLE_NAME_OVERALL, Items.ONE);
			
				
			case TRACKS_7DAY:
				return new UriData(TrackEntry.TABLE_NAME_SEVEN_DAY, Items.MANY);
		
			case TRACKS_7DAY_ID:
				return new UriData(TrackEntry.TABLE_NAME_SEVEN_DAY, Items.ONE);
		
				
			case TRACKS_1MONTH:
				return new UriData(TrackEntry.TABLE_NAME_ONE_MONTH, Items.MANY);
			
			case TRACKS_1MONTH_ID:
				return new UriData(TrackEntry.TABLE_NAME_ONE_MONTH, Items.ONE);
			
				
			case TRACKS_3MONTH:
				return new UriData(TrackEntry.TABLE_NAME_THREE_MONTH, Items.MANY);
		
			case TRACKS_3MONTH_ID:
				return new UriData(TrackEntry.TABLE_NAME_THREE_MONTH, Items.ONE);
			
			
			case TRACKS_6MONTH:
				return new UriData(TrackEntry.TABLE_NAME_SIX_MONTH, Items.MANY);
		
			case TRACKS_6MONTH_ID:
				return new UriData(TrackEntry.TABLE_NAME_SIX_MONTH, Items.ONE);
	
				
			case TRACKS_12MONTH:
				return new UriData(TrackEntry.TABLE_NAME_TWELVE_MONTH, Items.MANY);
			
				
			case TRACKS_12MONTH_ID:
				return new UriData(TrackEntry.TABLE_NAME_TWELVE_MONTH, Items.ONE);
	
			default:
				throw new UnsupportedOperationException("Unknown uri: " + uri);
		}
		
	}
	 
	private class UriData {
		
		private String table;
		private Items items;
		
		public UriData(String table, Items items){
			this.table = table;
			this.items = items;
		}
		
		public String getTable(){
			return this.table;
		}
		
		public Items getItems(){
			return this.items;
		}
	}

}
