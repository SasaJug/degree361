package com.sasaj.degree361taskapp.provider;

import java.io.File;

import com.sasaj.degree361taskapp.provider.TrackContract.TrackEntry;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class LastFmDatabaseHelper extends SQLiteOpenHelper {
    /**
     * If the database schema is changed, the database version must be
     * incremented.
     */
    private static final int DATABASE_VERSION = 3;

    /**
     * Database name.
     */
    public static final String DATABASE_NAME =
        "LastFm.db";

	public LastFmDatabaseHelper(Context context) {
		super(context, context.getCacheDir()
	              + File.separator 
	              + DATABASE_NAME,
	              null, 
	              DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		 // Define an SQL strings that create tables to hold Tracks for each period.
        final String SQL_CREATE_OVERALL_TABLE = "CREATE TABLE "
            + TrackEntry.TABLE_NAME_OVERALL + " (" 
            + TrackEntry._ID + " INTEGER PRIMARY KEY, " 
            + TrackEntry.COLUMN_NAME + " TEXT NOT NULL, "
            + TrackEntry.COLUMN_ARTIST + " TEXT NOT NULL, " 
            + TrackEntry.COLUMN_THUMBNAIL + " BLOB NOT NULL, " 
            + TrackEntry.COLUMN_URL + " TEXT NOT NULL" 
            + " );";
			
		final String SQL_CREATE_7DAY_TABLE = "CREATE TABLE "
            + TrackEntry.TABLE_NAME_SEVEN_DAY + " (" 
            + TrackEntry._ID + " INTEGER PRIMARY KEY, " 
            + TrackEntry.COLUMN_NAME + " TEXT NOT NULL, "
            + TrackEntry.COLUMN_ARTIST + " TEXT NOT NULL, " 
            + TrackEntry.COLUMN_THUMBNAIL + " BLOB NOT NULL, " 
            + TrackEntry.COLUMN_URL + " TEXT NOT NULL"  
            + " );";
					
		final String SQL_CREATE_1MONTH_TABLE = "CREATE TABLE "
            + TrackEntry.TABLE_NAME_ONE_MONTH + " (" 
            + TrackEntry._ID + " INTEGER PRIMARY KEY, " 
            + TrackEntry.COLUMN_NAME + " TEXT NOT NULL, "
            + TrackEntry.COLUMN_ARTIST + " TEXT NOT NULL, " 
            + TrackEntry.COLUMN_THUMBNAIL + " BLOB NOT NULL, "  
            + TrackEntry.COLUMN_URL + " TEXT NOT NULL" 
            + " );";
			
			
		final String SQL_CREATE_3MONTH_TABLE = "CREATE TABLE "
            + TrackEntry.TABLE_NAME_THREE_MONTH + " (" 
            + TrackEntry._ID + " INTEGER PRIMARY KEY, " 
            + TrackEntry.COLUMN_NAME + " TEXT NOT NULL, "
            + TrackEntry.COLUMN_ARTIST + " TEXT NOT NULL, " 
            + TrackEntry.COLUMN_THUMBNAIL + " BLOB NOT NULL, "  
            + TrackEntry.COLUMN_URL + " TEXT NOT NULL"  
            + " );";
			
			
		final String SQL_CREATE_6MONTH_TABLE = "CREATE TABLE "
            + TrackEntry.TABLE_NAME_SIX_MONTH + " (" 
            + TrackEntry._ID + " INTEGER PRIMARY KEY, " 
            + TrackEntry.COLUMN_NAME + " TEXT NOT NULL, "
            + TrackEntry.COLUMN_ARTIST + " TEXT NOT NULL, " 
            + TrackEntry.COLUMN_THUMBNAIL + " BLOB NOT NULL, " 
            + TrackEntry.COLUMN_URL + " TEXT NOT NULL"  
            + " );";
			
			
		final String SQL_CREATE_12MONTH_TABLE = "CREATE TABLE "
            + TrackEntry.TABLE_NAME_TWELVE_MONTH + " (" 
            + TrackEntry._ID + " INTEGER PRIMARY KEY, " 
            + TrackEntry.COLUMN_NAME + " TEXT NOT NULL, "
            + TrackEntry.COLUMN_ARTIST + " TEXT NOT NULL, " 
            + TrackEntry.COLUMN_THUMBNAIL + " BLOB NOT NULL, "  
            + TrackEntry.COLUMN_URL + " TEXT NOT NULL"  
            + " );";
        
        // Create the table.
        db.execSQL(SQL_CREATE_OVERALL_TABLE);
        db.execSQL(SQL_CREATE_7DAY_TABLE);
        db.execSQL(SQL_CREATE_1MONTH_TABLE);
        db.execSQL(SQL_CREATE_3MONTH_TABLE);
        db.execSQL(SQL_CREATE_6MONTH_TABLE);
        db.execSQL(SQL_CREATE_12MONTH_TABLE);

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int arg1, int arg2) {
        db.execSQL("DROP TABLE IF EXISTS " 
                + TrackEntry.TABLE_NAME_OVERALL);
        db.execSQL("DROP TABLE IF EXISTS " 
                + TrackEntry.TABLE_NAME_SEVEN_DAY);
        db.execSQL("DROP TABLE IF EXISTS " 
                + TrackEntry.TABLE_NAME_ONE_MONTH);
        db.execSQL("DROP TABLE IF EXISTS " 
                + TrackEntry.TABLE_NAME_THREE_MONTH);
        db.execSQL("DROP TABLE IF EXISTS " 
                + TrackEntry.TABLE_NAME_SIX_MONTH);
        db.execSQL("DROP TABLE IF EXISTS " 
                + TrackEntry.TABLE_NAME_TWELVE_MONTH);
     onCreate(db);

	}

}