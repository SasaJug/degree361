package com.sasaj.degree361taskapp.provider;


import android.net.Uri;
import android.provider.BaseColumns;

public class TrackContract {
	
	 /**
     * The "Content authority" is a name for the entire content
     * provider, similar to the relationship between a domain name and
     * its website. A convenient string to use for the content
     * authority is the package name for the app, which is guaranteed
     * to be unique on the device.
     */
    public static final String AUTHORITY = "com.sasaj.lastfm";
    
    /**
     * Inner class that defines the table contents of the Video
     * table.
     */
    public static final class TrackEntry implements BaseColumns {

        
        /**
         * Name of the database tables.
         */
		public static final String TABLE_NAME_OVERALL = "overall";
		public static final String TABLE_NAME_SEVEN_DAY = "seven_day";
		public static final String TABLE_NAME_ONE_MONTH = "one_month";
		public static final String TABLE_NAME_THREE_MONTH = "three_month";
		public static final String TABLE_NAME_SIX_MONTH = "six_month";
		public static final String TABLE_NAME_TWELVE_MONTH = "twelve_month";


        
		
        /**
         * Columns to store Data of each Track.
         */
		public static final String COLUMN_NAME = "name";
        public static final String COLUMN_ARTIST = "artist";
        public static final String COLUMN_THUMBNAIL = "thumbnail";
        public static final String COLUMN_URL = "url";

        /**
         * Return a Uri that points to the row containing a given id.
         * 
		 * @param table
         * @param id
         * @return Uri
         */
        public static Uri buildTrackUri(String table, Long id) {
            return  Uri.parse("content://" + AUTHORITY + "/" + table + "/"+Long.toString(id));
        }
    }  

}
