package com.sasaj.degree361taskapp.provider;


import java.io.InputStream;
import java.net.URL;

import com.sasaj.degree361taskapp.R;
import com.sasaj.degree361taskapp.common.ImageUtils;
import com.sasaj.degree361taskapp.models.Track;
import com.sasaj.degree361taskapp.provider.TrackContract.TrackEntry;
import com.sasaj.degree361taskapp.states.OneMonthState;
import com.sasaj.degree361taskapp.states.OverallState;
import com.sasaj.degree361taskapp.states.SevenDayState;
import com.sasaj.degree361taskapp.states.SixMonthState;
import com.sasaj.degree361taskapp.states.State;
import com.sasaj.degree361taskapp.states.ThreeMonthState;
import com.sasaj.degree361taskapp.states.TwelveMonthState;
import com.sasaj.degree361taskapp.webdata.LastFmApi;

import retrofit.RestAdapter;
import retrofit.RestAdapter.LogLevel;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;

public class Controller {
	
	public static enum Period {OVERALL, SEVEN_DAY, ONE_MONTH, THREE_MONTH, SIX_MONTH, TWELVE_MONTH}
	
	private final String TAG = getClass().getSimpleName();
	private Context mContext;
	

	private LastFmApi mLastFmApi;
	
	private State overallState;
	private State sevenDayState;
	private State oneMonthState;
	private State threeMonthState;
	private State sixMonthState;
	private State twelveMonthState;
	
	
	private State state;

	
	public Controller(Context context) { 
        // Store the Application Context.
        mContext = context;
        // Initialize the VideoServiceProxy.
        mLastFmApi = new RestAdapter
            .Builder()
            .setEndpoint(Constants.SERVER_URL)
            .setLogLevel(LogLevel.BASIC)
            .build()
            .create(LastFmApi.class);
        
        //Initializes State objects.
        overallState = new OverallState(this);
    	sevenDayState = new SevenDayState(this);
    	oneMonthState = new OneMonthState(this);
    	threeMonthState = new ThreeMonthState(this);
    	sixMonthState = new SixMonthState(this);
    	twelveMonthState = new TwelveMonthState(this);
        
        this.state = oneMonthState;
    }
	
	public void setState(State state){
		this.state=state;
	}
	
	public State getState() {
		return state;
	}

	public void setStateForPeriod(Period period){
		switch(period){
			case OVERALL:
				setState(overallState);
				break;
			case SEVEN_DAY:
				setState(sevenDayState);
				break;
			case ONE_MONTH:
				setState(oneMonthState);
				break;
			case THREE_MONTH:
				setState(threeMonthState);
				break;
			case SIX_MONTH:
				setState(sixMonthState);
				break;
			case TWELVE_MONTH:
				setState(twelveMonthState);
				break;
			default:
				setState(overallState);
		}
	}
	
	public Context getmContext() {
		return mContext;
	}

	public LastFmApi getmLastFmApi() {
		return mLastFmApi;
	}
	
	// Invokes syncTable() on current State object.
	// It starts two-way retrofit call and gets List of Tracks from LastFm service.
	// After that old data from corresponding table is deleted and new Tracks are inserted. 
	public boolean syncTable(){
		return state.syncTable();
	}
	
	// Invokes getTrackList() on current State object.
	// Then it queries corresponding table and returns Cursor.
	public Cursor getTracksList(){
		return state.getTracksList();
	}
	
	// Called from current State Object
	// Inserts new rows one by one into corresponding table. 
	public Uri insertRow(Uri contentUri, Track track) {
		
		Bitmap bitmap = null;
		
		try {
			String imageUrl = track.getImage().get(0).getText();
			InputStream in = (InputStream) new URL(imageUrl).getContent();
			bitmap = BitmapFactory.decodeStream(in);
			if (bitmap == null){
				bitmap = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.download_icon);				
			}
			in.close();
			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		
		ContentValues cv = new ContentValues();
		 cv.put(TrackEntry.COLUMN_NAME,track.getName());
         cv.put(TrackEntry.COLUMN_ARTIST,track.getArtist().getName());
         cv.put(TrackEntry.COLUMN_THUMBNAIL,ImageUtils.getBytes(bitmap));
         cv.put(TrackEntry.COLUMN_URL,track.getUrl());
		
		 return mContext.getContentResolver().insert(contentUri, cv);
	}



}
