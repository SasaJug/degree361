package com.sasaj.degree361taskapp.models;

import com.google.gson.annotations.Expose;

public class LastFm {
	
	@Expose
	private Toptracks toptracks;
	
	/**
	* 
	* @return
	* The toptracks
	*/
	public Toptracks getToptracks() {
	return toptracks;
	}
	
	/**
	* 
	* @param toptracks
	* The toptracks
	*/
	public void setToptracks(Toptracks toptracks) {
	this.toptracks = toptracks;
	}
	
	@Override
	public String toString() {
		return "TopTracks: "+ toptracks;
	}
}