package com.sasaj.degree361taskapp.models;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Toptracks {

	@Expose
	private List<Track> track = new ArrayList<Track>();
	@SerializedName("@attr")
	@Expose
	private Attr_ Attr;
	
	/**
	* 
	* @return
	* The track
	*/
	public List<Track> getTrack() {
	return track;
	}
	
	/**
	* 
	* @param track
	* The track
	*/
	public void setTrack(List<Track> track) {
	this.track = track;
	}
	
	/**
	* 
	* @return
	* The Attr
	*/
	public Attr_ getAttr() {
	return Attr;
	}
	
	/**
	* 
	* @param Attr
	* The @attr
	*/
	public void setAttr(Attr_ Attr) {
	this.Attr = Attr;
	}
	
	@Override
	public String toString() {
	return "Top Tracks: " + track.toString();
	}
}