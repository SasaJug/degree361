package com.sasaj.degree361taskapp.models;

import com.google.gson.annotations.Expose;

public class Attr_ {

	@Expose
	private String user;
	@Expose
	private String page;
	@Expose
	private String perPage;
	@Expose
	private String totalPages;
	@Expose
	private String total;
	
	/**
	* 
	* @return
	* The user
	*/
	public String getUser() {
	return user;
	}
	
	/**
	* 
	* @param user
	* The user
	*/
	public void setUser(String user) {
	this.user = user;
	}
	
	/**
	* 
	* @return
	* The page
	*/
	public String getPage() {
	return page;
	}
	
	/**
	* 
	* @param page
	* The page
	*/
	public void setPage(String page) {
	this.page = page;
	}
	
	/**
	* 
	* @return
	* The perPage
	*/
	public String getPerPage() {
	return perPage;
	}
	
	/**
	* 
	* @param perPage
	* The perPage
	*/
	public void setPerPage(String perPage) {
	this.perPage = perPage;
	}
	
	/**
	* 
	* @return
	* The totalPages
	*/
	public String getTotalPages() {
	return totalPages;
	}
	
	/**
	* 
	* @param totalPages
	* The totalPages
	*/
	public void setTotalPages(String totalPages) {
	this.totalPages = totalPages;
	}
	
	/**
	* 
	* @return
	* The total
	*/
	public String getTotal() {
	return total;
	}
	
	/**
	* 
	* @param total
	* The total
	*/
	public void setTotal(String total) {
	this.total = total;
	}


    @Override
    public String toString()
    {
        return "page = "+page+", perPage = "+perPage+", totalPages = "+totalPages +", total = "+total;
    }

}