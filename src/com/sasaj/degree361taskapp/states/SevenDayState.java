package com.sasaj.degree361taskapp.states;

import java.util.List;

import retrofit.RetrofitError;

import com.sasaj.degree361taskapp.models.LastFm;
import com.sasaj.degree361taskapp.models.Track;
import com.sasaj.degree361taskapp.provider.Controller;
import com.sasaj.degree361taskapp.provider.TrackContract;
import com.sasaj.degree361taskapp.provider.TrackContract.TrackEntry;

import android.database.Cursor;
import android.net.Uri;

public class SevenDayState implements State {

	private static final Uri CONTENT_URI =  Uri.parse("content://" + TrackContract.AUTHORITY + "/" + TrackEntry.TABLE_NAME_SEVEN_DAY);
	private Controller controller;
	private List<Track> mTrackList;

	public SevenDayState (Controller controller){
		this.controller = controller;
	}

	@Override
	public boolean syncTable() {
		try {
			LastFm lastFm = controller.getmLastFmApi().getTracksList7day();
			mTrackList = lastFm.getToptracks().getTrack();
		} catch (RetrofitError e) {
			return false;
		}
		
		controller.getmContext().getContentResolver().delete(CONTENT_URI,null,null);
		
		 for(Track track : mTrackList){
			 
				controller. insertRow (CONTENT_URI,track);

		 }
		 
		return true;
	}

	@Override
	public Cursor getTracksList() {
		// TODO Auto-generated method stub
		return controller.getmContext().getContentResolver().query(CONTENT_URI, null, null, null, null);
	}

}
