package com.sasaj.degree361taskapp.states;

import android.database.Cursor;

public interface State {
	
	public boolean syncTable();
	
	public Cursor getTracksList();

}
