package com.sasaj.degree361taskapp.view;

import com.sasaj.degree361taskapp.R;
import com.sasaj.degree361taskapp.common.Utils;
import com.sasaj.degree361taskapp.view.adapters.LastFmPagerAdapter;
import com.sasaj.degree361taskapp.view.ui.TrackListFragment;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.util.Log;

public class MainActivity extends FragmentActivity{

	ViewPager viewPager = null;
	private final String TAG = getClass().getSimpleName();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
		FragmentManager fragmentManager = super.getSupportFragmentManager();
		viewPager.setAdapter(new LastFmPagerAdapter(fragmentManager));
		checkNetworkConnection();
		
	}
	
	
	public void checkNetworkConnection(){
		Log.i(TAG, "Checking connection");
		ConnectivityManager cm =
		(ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);

		NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
		boolean con = activeNetwork != null &&
							  activeNetwork.isConnectedOrConnecting();
		TrackListFragment.isConnected =con;
		if(!con)
			Utils.showToast(getApplicationContext(), "No internet connection");
			
	
	}
}