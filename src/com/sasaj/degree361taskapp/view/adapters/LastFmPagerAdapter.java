package com.sasaj.degree361taskapp.view.adapters;

import com.sasaj.degree361taskapp.provider.Controller.Period;
import com.sasaj.degree361taskapp.view.ui.TrackListFragment;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class LastFmPagerAdapter extends FragmentStatePagerAdapter {

	private static int NUM_ITEMS = 6;
	
	public LastFmPagerAdapter(FragmentManager fm){
		super(fm);
	}
	
	@Override
	public Fragment getItem(int position){
		
		
		 switch (position) {
           case 0: 
               return TrackListFragment.newInstance(Period.OVERALL);
           case 1: 
               return TrackListFragment.newInstance(Period.SEVEN_DAY);
           case 2:
               return TrackListFragment.newInstance(Period.ONE_MONTH);
           case 3:
               return TrackListFragment.newInstance(Period.THREE_MONTH);
           case 4:
               return TrackListFragment.newInstance(Period.SIX_MONTH);
           case 5:
               return TrackListFragment.newInstance(Period.TWELVE_MONTH);
           default:
           		return null;
       }
	}

	@Override
	public int getCount(){
		return NUM_ITEMS;
	}

	@Override
	public CharSequence getPageTitle(int position){
		
		switch (position) {
            case 0: 
                return "Overall";
            case 1: 
                return "Last 7 Days";
            case 2: 
                return "Last Month";
            case 3:
                return "Last Three Months";
            case 4:
                return "Last Six Months";
            case 5:
                return "Last Year";
            default:
            	return null;
        }
		
	}
}