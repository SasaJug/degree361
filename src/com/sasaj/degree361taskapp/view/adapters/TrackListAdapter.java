package com.sasaj.degree361taskapp.view.adapters;

import java.io.InputStream;
import java.net.URL;

import com.sasaj.degree361taskapp.R;
import com.sasaj.degree361taskapp.common.ImageUtils;
import com.sasaj.degree361taskapp.provider.TrackContract.TrackEntry;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v4.widget.CursorAdapter;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;



public class TrackListAdapter extends SimpleCursorAdapter {

	/**
     * Allows access to application-specific resources and classes.
     */

	static String[] from = new String[] { TrackEntry.COLUMN_NAME, TrackEntry.COLUMN_ARTIST };
	static int[] to = new int[] {R.id.trackName, R.id.artistName};
	private static Cursor cursor;
	private View trackView;

    //Constructor
    public TrackListAdapter(Context context) {
   
		super(context, R.layout.single_track_view, cursor, from, to, CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);

	}


	@Override
	public View newView(Context context, Cursor cursor, ViewGroup parent) {
		
		final LayoutInflater inflater = LayoutInflater.from(context);
		View v = inflater.inflate(R.layout.single_track_view, parent, false);
		return v;
		
	}

	
	@Override
	public void bindView(View view, Context context, Cursor cursor) {
		
		trackView = view;
		
		int nameCol = cursor.getColumnIndex(TrackEntry.COLUMN_NAME);
		String name = cursor.getString(nameCol);
		
		int artistCol = cursor.getColumnIndex(TrackEntry.COLUMN_ARTIST);
		String artist = cursor.getString(artistCol);
		
		Bitmap bm = ImageUtils.getPhoto(cursor.getBlob(cursor.getColumnIndex(TrackEntry.COLUMN_THUMBNAIL)));
	
		TextView name_text = (TextView) view.findViewById (R.id.trackName);
		name_text.setText(name);
		
		TextView artist_text = (TextView) view.findViewById (R.id.artistName);
		artist_text.setText(artist);
		
		ImageView image = (ImageView) view.findViewById(R.id.icon);
		image.setImageBitmap(bm);
	} 

	
}