package com.sasaj.degree361taskapp.view.ui;

import com.sasaj.degree361taskapp.R;
import com.sasaj.degree361taskapp.common.Utils;
import com.sasaj.degree361taskapp.provider.Controller.Period;
import com.sasaj.degree361taskapp.provider.TrackContract.TrackEntry;
import com.sasaj.degree361taskapp.view.MainActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;



import com.sasaj.degree361taskapp.provider.Controller;
import com.sasaj.degree361taskapp.view.adapters.TrackListAdapter;

import android.os.AsyncTask;

public class TrackListFragment extends ListFragment{
	
	protected static final String CHOOSER_TEXT = "Choose action";
	private final String TAG = getClass().getSimpleName();
	public static boolean isConnected = true;
	protected static boolean isServiceAvailable = true;
	
	private Period period;
	private MainActivity myActivity = null;
	private TrackListAdapter mAdapter;
	private ListView mTracksList;
	private Cursor cursor;
	private boolean firstTimeIn = true;
	
	// newInstance constructor for creating fragment with arguments
	public static TrackListFragment newInstance(Period period) {
		TrackListFragment trackListFragment = new TrackListFragment();
		Bundle args = new Bundle();
		args.putSerializable("period", period);
		trackListFragment.setArguments(args);
		return trackListFragment;
	}

	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		this.myActivity = (MainActivity) getActivity();
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		if (savedInstanceState != null)
			firstTimeIn = false;
		else
			firstTimeIn = true;
		
		period = (Period) getArguments().get("period");
		TaskParams params = new TaskParams(myActivity.getApplicationContext(), period);
		GetTrackListTask task = new GetTrackListTask();
        task.execute(params);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, 
            Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.track_list_fragment, container, false);
		return view;
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		
		super.onActivityCreated(savedInstanceState);
		mAdapter = new TrackListAdapter(getActivity());
		mTracksList = getListView();
		setListAdapter(mAdapter);
		mTracksList.setOnItemClickListener(new OnItemClickListener() {
	       	
	       	@Override
	       	   public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
	       	 cursor.moveToPosition(position);
           	 Uri webpage = Uri.parse(cursor.getString(cursor.getColumnIndex(TrackEntry.COLUMN_URL)));
             Intent baseIntent = new Intent(Intent.ACTION_VIEW, webpage);
             
      		String title = CHOOSER_TEXT;
      		// Create intent to show chooser
      		Intent chooserIntent = Intent.createChooser(baseIntent, title);

      		// Verify the intent will resolve to at least one activity
      		if (baseIntent.resolveActivity(myActivity.getPackageManager()) != null) {
      			myActivity.startActivity(chooserIntent);
      		}
				
					}

	       	});
	}
	
	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
	}
	
	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
		myActivity = null;
	}
	
	public Period getPeriod(){
		return this.period;
	}
	
	public void displayList(Cursor cursor){
		this.cursor= cursor;
		if (mAdapter != null)
			mAdapter.swapCursor(cursor);
	}



	// Sends video rating to server
	private class GetTrackListTask extends AsyncTask<TaskParams, Void, Cursor>{



		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
		}

		@Override
		protected Cursor doInBackground(TaskParams... params) {
			
			Controller mController = new Controller(params[0].context);
			mController.setStateForPeriod(params[0].period);
			
			
			if (firstTimeIn && isConnected){
				Log.i(TAG, "executing syncTable()");
				mController.syncTable();
			}
				
			
			return mController.getTracksList();
		}

		@Override
		protected void onPostExecute(Cursor result) {
			if (result != null)
				displayList(result);
		}
		
	}


	private class TaskParams{
		public Period period;
		public Context context;
		
		public TaskParams(Context context, Period period){
			this.context = context;
			this.period = period;
		}
	}
	
	
}