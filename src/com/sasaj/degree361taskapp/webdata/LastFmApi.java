package com.sasaj.degree361taskapp.webdata;


import com.sasaj.degree361taskapp.models.LastFm;

import retrofit.http.GET;


public interface LastFmApi {
	public static final String USER = "joi";
	public static final String METHOD = "user.getTopTracks";
	public static final String API_KEY = "51de025812af79cb70f4a872936181a0";
	public static final String FORMAT = "json";
	
	// Period constants
	public static final String PERIOD_OVERALL = "overall";
	public static final String PERIOD_7DAY = "7day";
	public static final String PERIOD_1MONTH = "1month";
	public static final String PERIOD_3MONTH = "3month";
	public static final String PERIOD_6MONTH = "6month";
	public static final String PERIOD_12MONTH = "12month";

	
    /**
     * Sends a GET request to get the List of ALL-TIME Top Tracks user from lastFm
     * Web service using a two-way Retrofit RPC call.
     */
    @GET("/?method="+METHOD+"&api_key="+API_KEY+"&user="+USER+"&period="+PERIOD_OVERALL+"&format="+FORMAT)
    public LastFm getTracksListOverall();
    
	
    /**
     * Sends a GET request to get the List of 7DAY Top Tracks user from lastFm
     * Web service using a two-way Retrofit RPC call.
     */
    @GET("/?method="+METHOD+"&api_key="+API_KEY+"&user="+USER+"&period="+PERIOD_7DAY+"&format="+FORMAT)
    public LastFm getTracksList7day();
    
	
    /**
     * Sends a GET request to get the List of 1MONTH Top Tracks user from lastFm
     * Web service using a two-way Retrofit RPC call.
     */
    @GET("/?method="+METHOD+"&api_key="+API_KEY+"&user="+USER+"&period="+PERIOD_1MONTH+"&format="+FORMAT)
    public LastFm getTracksList1month();
    
	
    /**
     * Sends a GET request to get the List of 3MONTH Top Tracks user from lastFm
     * Web service using a two-way Retrofit RPC call.
     */
    @GET("/?method="+METHOD+"&api_key="+API_KEY+"&user="+USER+"&period="+PERIOD_3MONTH+"&format="+FORMAT)
    public LastFm getTracksList3month();
    
	
    /**
     * Sends a GET request to get the List of 6MONTH Top Tracks user from lastFm
     * Web service using a two-way Retrofit RPC call.
     */
    @GET("/?method="+METHOD+"&api_key="+API_KEY+"&user="+USER+"&period="+PERIOD_6MONTH+"&format="+FORMAT)
    public LastFm getTracksList6month();
    
	
    /**
     * Sends a GET request to get the List of 12MONTH Top Tracks user from lastFm
     * Web service using a two-way Retrofit RPC call.
     */
    @GET("/?method="+METHOD+"&api_key="+API_KEY+"&user="+USER+"&period="+PERIOD_12MONTH+"&format="+FORMAT)
    public LastFm getTracksList12month();

}
 // http://ws.audioscrobbler.com/2.0/?method=chart.getTopTracks&api_key=51de025812af79cb70f4a872936181a0&format=json